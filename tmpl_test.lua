vars = {
  test = "value",
  test2 = "value2",
}

t = require("tmpl")

io.write("vars\n")
io.write("---\n")
io.write(t.parse("tmpl_test.txt", vars))
io.write("---\n")
vars.print = print
io.write("vars with print in env\n")
io.write("---\n")
io.write(t.parse("tmpl_test.txt", vars))
io.write("---\n")
io.write("vars file\n")
io.write("---\n")
io.write(t.parse("tmpl_test.txt", "tmpl_test_vars.lua"))
io.write("---\n")
io.write("vars file with print in env\n")
io.write("---\n")
io.write(t.parse("tmpl_test.txt", "tmpl_test_vars.lua", {print = print}))
io.write("---\n")
io.write("vars file with env\n")
io.write("---\n")
io.write(t.parse("tmpl_test.txt", "tmpl_test_vars.lua", _ENV))
io.write("---\n")

