--[[
https://gitlab.com/mdkmde/tmpl_lua/ - matthias (at) koerpermagie.de

Copyright (c) ISC License - Matthias Diener

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
]]

local load, pcall, setmetatable, type = load, pcall, setmetatable, type
local io = {open = io.open}
local string = {find = string.find, format = string.format, gsub = string.gsub, rep = string.rep}
local table = {concat = table.concat}
_ENV = nil

local getFileContent = function(is_filename)
-- returns content string or nil, error message
  local _, f_content = pcall(io.open, is_filename, "r")
  if f_content then
    local s_content = f_content:read("a") -- "*a" for Lua 5.2
    f_content:close()
    if s_content then
      return s_content
    end
  end
  return nil, string.format("ERROR: Cannot %s file: %s", f_content and "read" or "open", is_filename)
end

local loadData = function(is_data, is_filename, it_env)
-- returns loaded data (table/string) or nil, error message, input data string
  local data, s_err = load(is_data, is_filename, "t", it_env)
  if data then
    local b_parse_ok, st_data = pcall(data)
    if b_parse_ok then
      return st_data
    end
    return nil, string.format("ERROR: %s", st_data), is_data
  end
  return nil, string.format("ERROR: Cannot load: %s", s_err), is_data
end

local getVars = function(ist_vars, it_env)
-- returns var table or nil, error message
  if type(ist_vars) == "string" then
    local s_vars, s_err = getFileContent(ist_vars)
    if s_vars then
      return loadData(string.format("%s; return _ENV", s_vars), ist_vars, it_env or {})
    end
    return nil, s_err
  end
  return it_env and setmetatable(ist_vars or {}, {__index = it_env}) or ist_vars or {}
end

local getLbl = function(is_tout)
-- returns long bracket level (=*)
  local t_lbl = {}
  string.gsub(is_tout, "[][]?([][]=*)", function(is_lbl)
    t_lbl[#is_lbl] = true
  end)
  return string.rep("=", #t_lbl)
end

local getCrnl = function(is_tout)
-- returns eol marker
  local crnl
  if string.find(is_tout, "\r") then
    if string.find(is_tout, "\r\n") then
      crnl = "\r\n"
    else
      crnl = "\r"
    end
  end
  return crnl or "\n"
end

local parse = function (is_tmpl, ist_vars, it_env)
-- returns output string or nil, error message, prepared template
  local t_vars, s_err = getVars(ist_vars, it_env)
  if not(t_vars) then return nil, s_err; end
  local s_tout, s_err = string.gsub(is_tmpl, "^|", "")
  if s_err == 0 then
    s_tout, s_err = getFileContent(is_tmpl)
  end
  if not(s_tout) then return nil, s_err; end
  local crnl = getCrnl(s_tout)
  local lbl = getLbl(s_tout, "")
  s_tout = string.format("local s_out = [%s[%s]%s]%s return s_out", lbl, s_tout, lbl, crnl)
  local t_tmpl_replace_escape = {}
  local n_escape = 0
  s_tout = string.gsub(s_tout, "{{ *([{}%%]+) *}}( *\r?\n?)", function(is_escape, is_crnl)
    n_escape = n_escape + 1
    t_tmpl_replace_escape[n_escape] = is_escape
    return string.format("]%s] .. (t_tmpl_replace_escape[%i]) .. [%s[%s%s", lbl, n_escape, lbl, is_crnl, is_crnl)
  end)
  -- replace code segment
  s_tout = string.gsub(s_tout, "(\r?\n?)([ \t]*){%% *", function(is_crnl, is_space) return string.format("%s]%s]%s ", is_crnl == "" and is_space or is_crnl, lbl, crnl) end)
  s_tout = string.gsub(s_tout, " *%%}( *\r?\n?)", string.format("%s s_out = s_out .. [%s[%%1", crnl, lbl))
  -- replace variable
  s_tout = string.gsub(s_tout, "{{ *", string.format("]%s] .. (", lbl))
  s_tout = string.gsub(s_tout, " *}}( *\r?\n?)", string.format(") .. [%s[%%1%%1", lbl))
  if 0 < n_escape then
    s_tout = string.format("local t_tmpl_replace_escape = {\"%s\"}%s%s", table.concat(t_tmpl_replace_escape, "\",\""), crnl, s_tout)
  end
  return loadData(s_tout, is_tmpl, t_vars)
end

return {
  parse = parse, --[[ (template file name / "|template" string,
      env table or file name,
      env table for loading env file [arg2])
    returns output string or nil, error message, prepared template ]]
}

